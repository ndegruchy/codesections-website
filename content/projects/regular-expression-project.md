+++
title = "Reg Experiment"
date = 2018-06-03T13:39:00-04:00
weight = 4
[extra]
demo_link = "/projects/reg-exp/" 
+++

A clone of [RegExceptional](http://regexmatch.herokuapp.com/#/), a HackReactor final project that consists of a matching game to help teach regular expressions.  My goal is to show that we can get exactly the same functionality without any frameworks and that the resulting site will not only look nearly identical, but will also load less data and do so faster.  The clone I built reduces the initial data transfer by 99.83%, and cuts the page load time from 600ms to right around 100ms, which I'm prepared to score as a victory.

<!--more-->
